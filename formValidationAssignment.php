<?php
	
	// Variables for error and confirmation messages.
	$errorMsgName = "";
	$errorMsgNumber = "";
	$errorMsgResponse = "";
	$confirmationMsg = "";
	
	//Variables for form data.
	$customerName = "";
	$customerSNumber = "";
	$customerResponse = "";
	
	$validForm = false;
	
	if(isset($_POST["submit"]))
	{
		// Get information from form fields and assign to variables.
		$customerName = trim($_POST["inName"]); //Trim will remove any leading or trailing spaces.
		$customerSNumber = $_POST["inNumber"];
		$customerResponse = $_POST["RadioGroup1"];
		
		$confirmationMsg = "";
		$validForm = true;
		
		validateName();
		validateNumber();
		validateResponse();
		
		if($validForm)
		{
			$confirmationMsg = "The information above has passed validation.";
		}
	}
	else
	{
		// Displays the empty form if submit button has not been pressed.
	}
	
	function validateName()
	{
		global $customerName, $validForm, $errorMsgName;
		$errorMsgName = "";
		
		if($customerName == "")
		{
			$validForm = false;
			$errorMsgName = "Please enter a name.";
		}
	}
	
	function validateNumber()
	{
		global $customerSNumber, $validForm, $errorMsgNumber;
		$errorMsgNumber = "";
		
		// Verifies if 9 numbers entered with no special characters.
		if(!preg_match("/^\d{9}$/",$customerSNumber))
		{
			$validForm = false;
			$errorMsgNumber = "Must be 9 digits, with no special characters.";
		}
	}
	
	function validateResponse()
	{
		global $customerResponse, $validForm, $errorMsgResponse;
		$errorMsgResponse = "";
		
		if($customerResponse == "") // one must be chosen
		{
			$validForm = false;
			$errorMsgResponse = "Please choose a Response.";
		}
	}

?>
<!DOCTYPE html>
<html >
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WDV341 Intro PHP - Form Validation Example</title>
	<script>
		// Checks to see if honeypot field is empty. If empty, submits form, overwise if there's
		// a value then it's a bot and returns false and will not submit the form.
		function validateMyForm() 
		{
			if(!document.getElementById("honeypot").value) { 
				return true;
			} 
			else {
				return false;
			}
		}
	</script>
	<style>
		#orderArea	{
			width:600px;
			background-color:#CF9;
		}

		.error	{
			color:red;
			font-style:italic;	
		}
		
		#honeypotDiv {
			display: none;
		}
		
		.confirmation {
			color:blue;
			font-style:italic;
		}
	</style>
	</head>
	<body>
		<h1>WDV341 Intro PHP</h1>
		<h2>Form Validation Assignment</h2>
		<div id="orderArea">
			<form id="form1" onsubmit="return validateMyForm();" name="form1" method="post" action="formValidationAssignment.php">
				<h3>Customer Registration Form</h3>
				<table width="587" border="0">
					<tr>
						<td width="117">Name:</td>
						<td width="246"><input type="text" name="inName" id="inName" size="40" 
							value="<?php echo $customerName; ?>"/></td>
						<td width="210" class="error"><?php echo $errorMsgName; ?></td>
					</tr>
					<tr>
						<td>Social Security</td>
						<td><input type="text" name="inNumber" id="inNumber" size="40" 
							value="<?php echo $customerSNumber; ?>" /></td>
						<td class="error"><?php echo $errorMsgNumber; ?></td>
					</tr>
					<tr>
						<td>Choose a Response</td>
						<td><p>
							<label>
								<input type="radio" name="RadioGroup1" id="RadioGroup1_0" value="phone" 
								<?php if(isset($customerResponse) && $customerResponse == "phone") echo "checked";?> >
							Phone</label>
							<br>
							<label>
								<input type="radio" name="RadioGroup1" id="RadioGroup1_1" value="email" 
								<?php if(isset($customerResponse) && $customerResponse == "email") echo "checked";?> >
							Email</label>
							<br>
							<label>
								<input type="radio" name="RadioGroup1" id="RadioGroup1_2" value="usMail" 
								<?php if(isset($customerResponse) && $customerResponse == "usMail") echo "checked";?> >
							US Mail</label>
							<br>
						</p></td>
						<td class="error"><?php echo $errorMsgResponse; ?></td>
					</tr>
				</table>
				<div id="honeypotDiv"> <!-- honeypot field -->
					<input type="text" name="honeypot" id="honeypot" />
				</div>
				<p>
					<input type="submit" name="submit" id="button" value="Register" />
					<input type="reset" name="button2" id="button2" value="Clear Form" />
					<span class="confirmation"><?php echo $confirmationMsg; ?></span>
				</p>
			</form>
		</div>
	</body>
</html>