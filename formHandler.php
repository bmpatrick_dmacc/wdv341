<?php
	
	
	//Model-Controller Area.  The PHP processing code goes in this area. 

	//Method 1.  This uses a loop to read each set of name-value pairs stored in the $_POST array
	$tableBody = "";		//use a variable to store the body of the table being built by the script
	
	foreach($_POST as $key => $value)		//This will loop through each name-value in the $_POST array
	{
		$tableBody .= "<tr>";				//formats beginning of the row
		$tableBody .= "<td>$key</td>";		//display the name of the name-value pair from the form
		$tableBody .= "<td>$value</td>";	//display the value of the name-value pair from the form
		$tableBody .= "</tr>";				//End this row
	} 
		
	if(isset($_POST['button'])) 
	{	
		//Method 2.  This method pulls the individual name-value pairs from the $_POST using the name
		//as the key in an associative array.  
		
		$inFirstName = $_POST["firstName"];		//Get the value entered in the first name field
		$inLastName = $_POST["lastName"];		//Get the value entered in the last name field
		$inSchool = $_POST["school"];			//Get the value entered in the school field
		$inStudy = $_POST["areaOfStudy"];		//Get the value entered of the selected radio button
		$inCourse = "";							//Variable to hold value of checkboxes - see if statement below
		$inGradYear = $_POST["gradYear"];		//Get the value entered of the selected dropdown item
		
		if(!empty($_POST["courses"]))			//Get the value entered of any selected checkboxes
		{
			foreach($_POST["courses"] as $selected)
			{
				$inCourse = $inCourse . " " . $selected;
			}
		}
		else
		{
			$inCourse = "None";
		}
	}
	
	if(isset($_POST['commentButton'])) 
	{
		// Implement the Email class for the Contact Form with Email assignment.
		include 'Emailer.php';
		
		$email = new Emailer();
		$email->setSentFrom("webform@brandonpatrick.info");
		$email->setSendTo("bmpatrick@dmacc.edu");
		$email->setEmailSubject("Comments");
		$email->setEmailMsg($_POST["comments"]);

		$confirmEmail = new Emailer();
		$confirmEmail->setSentFrom("webform@brandonpatrick.info");
		$confirmEmail->setSendTo($_POST["contactEmailaddress"]);
		$confirmEmail->setEmailSubject("Your comments were received.");
		$confirmEmail->setEmailMsg("Thank you for your feedback. We will respond soon.");
		
		$messageSent = "";
		if($email->sendEmail() == true)
		{
				$messageSent = "The message has been sent.";

		}
		else
		{
			$messageSent = "The message failed to send.";
		}
	}
?>
<!DOCTYPE html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>WDV 341 Intro PHP - Code Example</title>
	</head>

	<body>
		<h1>WDV341 Intro PHP</h1>
		<h2>Form Handler Result Page - Code Example</h2>
		<p>This page displays the results of the Server side processing. </p>
		<p>The PHP page has been formatted to use the Model-View-Controller (MVC) concepts. </p>
		<h3>Display the values from the form using Method 1. Uses a loop to process through the $_POST array</h3>
		<p>
			<table border='a'>
			<tr>
				<th>Field Name</th>
				<th>Value of Field</th>
			</tr>
			<?php echo $tableBody;  ?>
			</table>
		</p>
		<?php 
			if(isset($_POST['button']))
			{
		?>		
		<h3>Display the values from the form using Method 2. Displays the individual values.</h3>
		<p>School: <?php echo $inSchool; ?></p>
		<p>First Name: <?php echo $inFirstName; ?></p>
		<p>Last Name: <?php echo $inLastName; ?></p>
		<p>Area of Study: <?php echo $inStudy; ?></p>
		<p>Taking Courses: <?php echo $inCourse; ?></p>
		<p>Graduation Year: <?php echo $inGradYear; ?></p>
		<?php 
			}
			else
			{
		?>
		<h3>Confirmation Message: <?php echo $messageSent; ?></h3>
		<?php
			}
		?>
	</body>
</html>
