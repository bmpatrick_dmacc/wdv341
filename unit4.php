<!doctype html>

<?php
function dateMonthNumToText($month) {  //If number entered for month converted to string of month name
				

				if (is_numeric($month)) { 
						$dateObj   = DateTime::createFromFormat('!m', $month);
						
						$convertedMonth = $dateObj->format('F');
						
						return $convertedMonth;
				
				}
				
				else {
						
						return $month;
				
				}
		
		}

		function monthDayYearDateFormat ($day, $month, $year) {    // Places input in mm/dd/yyyy format
			
						$inputDate = $day . " " . dateMonthNumToText($month) . " " . $year;
			
						echo date("m/d/Y", strtotime($inputDate));
		}
		
		
		function dayMonthYearDateFormat ($day, $month, $year) {    // places input in dd/mm/yyyy format
			
						$inputDate = $day . " " . dateMonthNumToText($month) . " " . $year;
						
						echo date("d/m/Y", strtotime($inputDate));
		}
		
		
		function stringInfo ($inputString) { 
			
						$trimmedString = trim($inputString); // Removes any leading or trailing whitespace
						
						$stringLowercaseConvert = strtolower($inputString);  // Converts the string to lowercase
						
						$matchPattern = "DMACC";  // String that will be used by the preg_match function below for searching through the input string
						
						
						//String information display
			
						echo "<p>The string you entered:</p><h4>". $inputString . "</h4>";  
		
						echo "<p>The string you entered has <strong>" . strlen($inputString) ."</strong> characters</p>";

						echo "<p>Eliminating leading and trailing whitespace from the string results in <strong>" . strlen($trimmedString) . "</strong> characters.</p>";
						
						echo "<p>The string converted to all lowercase:</p><h4>". $stringLowercaseConvert ."</h4>";
						
						
						 if (preg_match("/$matchPattern/i", $inputString)) {  // Searches through the input string for the pattern defined by '$matchPattern'.  Displays whether it is found or not.
							   
							   echo "<p class = 'underBorder'><strong>" . $matchPattern . "</strong> was found within the input string.</p>";
							   
						 } else {
								
								echo "<p class = 'underBorder'><strong>" . $matchPattern . "</strong> was not found within the input string.</p>";
								
						 }
		}	

		
		function formatNumber($inputNumber) {  // Formats the input number
		
						echo number_format($inputNumber);
		
		}
		
		
		function formatCurrency($inputCurrency) {  // Converts the input number to US currency
						
						echo   "$" . number_format($inputCurrency, 2);
		
		}

?>

<html>
<head>
<meta charset="utf-8">
<title>PHP Functions</title>

</head>

<body>
<h1>Intro to PHP Assignment: PHP Functions</h1>
				
				
				<div>
			
						<p>Your entered date in mm/dd/yyyy format:</p>
				
						<h3>
				
								<?php 
										monthDayYearDateFormat(  
												$_POST['day'],
												$_POST['month'],
												$_POST['year']
										);	
								?>
					
						</h3>

				
				
						<p>Your entered date in dd/mm/yyyy format:</p>
						
						<h3>
						
								<?php
											dayMonthYearDateFormat(
													$_POST['day'],
													$_POST['month'],
													$_POST['year']
											);
								?>
							
						</h3>
						
								<?php
										stringInfo($_POST['inputString']);
								?>
						
						
						<p>Your number as a formatted number:</p>
						
						<h3>
						
								<?php
										formatNumber($_POST['inputNumber']);
								?>
							
						</h3>
						
						<p>Input number converted to US currency:</p>
						
						<h3>
								<?php
										formatCurrency($_POST['inputCurrency']);
								?>
						</h3>
						
				</div>	
</body>
</html>